import { isYearValid } from '../../src/helpers/validateExpYear';

describe('isYearValid', () => {
  it('should return true for a valid year', () => {
    const validYear = '2023';
    expect(isYearValid(validYear)).toBe(true);
  });

  it('should return false for a year with less than 4 digits', () => {
    const invalidYear = '20';
    expect(isYearValid(invalidYear)).toBe(false);
  });

  it('should return false for a year with more than 4 digits', () => {
    const invalidYear = '20230';
    expect(isYearValid(invalidYear)).toBe(false);
  });

  it('should return false for a year in the past', () => {
    const pastYear = '2021';
    expect(isYearValid(pastYear)).toBe(false);
  });

  it('should return false for a year more than 5 years in the future', () => {
    const futureYear = '2030';
    expect(isYearValid(futureYear)).toBe(false);
  });
});
