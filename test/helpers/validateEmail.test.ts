import { isEmailValid } from '../../src/helpers/validateEmail';

describe('isEmailValid', () => {
  it('should return true for a valid email address', () => {
    expect(isEmailValid('test@gmail.com')).toBe(true);
    expect(isEmailValid('test@hotmail.com')).toBe(true);
    expect(isEmailValid('test@yahoo.es')).toBe(true);
  });

  it('should return false for an email address with less than 5 characters', () => {
    expect(isEmailValid('a@b.c')).toBe(false);
  });

  it('should return false for an email address with more than 100 characters', () => {
    const longEmail = 'a'.repeat(101) + '@gmail.com';
    expect(isEmailValid(longEmail)).toBe(false);
  });

  it('should return false for an invalid email address', () => {
    expect(isEmailValid('invalidemail')).toBe(false);
  });
});
