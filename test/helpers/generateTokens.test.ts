import { generateRandomToken } from '../../src/helpers/generateTokens';

describe('generateRandomToken', () => {
  it('should generate a token of the specified length', () => {
    const length = 16;
    const token = generateRandomToken(length);

    expect(token.length).toBe(length);
  });

  it('should generate a unique token each time', () => {
    const length = 16;
    const token1 = generateRandomToken(length);
    const token2 = generateRandomToken(length);

    expect(token1).not.toBe(token2);
  });
});
