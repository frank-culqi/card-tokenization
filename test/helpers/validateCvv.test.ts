import { isCVVValid } from '../../src/helpers/validateCvv';


describe('isCVVValid', () => {
  it('should return true for a valid CVV (3 or 4 digits)', () => {
    expect(isCVVValid(123)).toBe(true);
    expect(isCVVValid(1234)).toBe(true);
  });

  it('should return false for a CVV with less than 3 digits', () => {
    expect(isCVVValid(12)).toBe(false);
  });

  it('should return false for a CVV with more than 4 digits', () => {
    expect(isCVVValid(12345)).toBe(false);
  });

  it('should return false for a non-numeric input', () => {
    const nonNumericInput = 'abc';
    const nonNumericInput2 = '123';
    const cvv = parseInt(nonNumericInput, 10);
    const cvv2 = parseInt(nonNumericInput2, 10);

    expect(isCVVValid(cvv)).toBe(false);
    expect(isCVVValid(cvv2)).toBe(true);
  });
});
