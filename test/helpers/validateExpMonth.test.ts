import { isMonthValid } from '../../src/helpers/validateExpMonth';

describe('isMonthValid', () => {
  it('should return true for a valid month (1-12)', () => {
    expect(isMonthValid('1')).toBe(true);
    expect(isMonthValid('12')).toBe(true);
  });

  it('should return false for a month less than 1', () => {
    expect(isMonthValid('0')).toBe(false);
  });

  it('should return false for a month greater than 12', () => {
    expect(isMonthValid('13')).toBe(false);
  });

  it('should return false for a non-numeric input', () => {
    expect(isMonthValid('abc')).toBe(false);
  });
});
