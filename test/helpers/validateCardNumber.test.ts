import { checkCardNumber } from '../../src/helpers/validateCardNumber';

describe('checkCardNumber', () => {
  it('should return true for a valid card number', () => {
    expect(checkCardNumber(4111111111111111)).toBe(true);
  });

  it('should return false for a card number with less than 13 digits', () => {
    expect(checkCardNumber(123456789012)).toBe(false);
  });

  it('should return false for a card number with more than 16 digits', () => {
    expect(checkCardNumber(41111111111111112222)).toBe(false);
  });

  it('should return false for an invalid card number', () => {
    expect(checkCardNumber(4111111111111112)).toBe(false);
  });
});
