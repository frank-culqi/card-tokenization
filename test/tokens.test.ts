import { tokens } from '../src/tokens';

describe('tokens', () => {
  it('should return a valid response for a valid request', async () => {
    const event = {
      body: JSON.stringify({
        card_number: '4111111111111111',
        cvv: '123',
        expiration_month: '12',
        expiration_year: '2023',
        email: 'test@gmail.com',
      }),
      headers: {
        authorization: 'Bearer pk_test_LsRBKejzCOEEWOsw',
      },
    };

    const response = await tokens(event);

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.body)).toEqual(expect.objectContaining({
      message: 'Token creados correctamente',
      data: expect.objectContaining({
        card_number: "4111111111111111",
        expiration_month: "12",
        expiration_year: "2023",
        email: "test@gmail.com",
      }),
    }));
  });

});
