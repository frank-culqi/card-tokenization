import sequelize from './config/pgsql';

async function testConnection() {
  try {
    await sequelize.authenticate();
    console.log('Conexión establecida correctamente.');
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
  }
}
testConnection().then(() => process.exit());
export default testConnection;