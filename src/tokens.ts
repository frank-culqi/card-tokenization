import { APIGatewayEvent, APIGatewayProxyResult } from 'aws-lambda';
import * as dotenv from 'dotenv';
import Redis from "ioredis";
import { redisCreate, redisQuit } from "./config/redis";
import CardTokens from "./models/cardTokens";
import { checkCardNumber } from './helpers/validateCardNumber';
import { isCVVValid } from './helpers/validateCvv';
import { isMonthValid } from "./helpers/validateExpMonth";
import { isYearValid } from "./helpers/validateExpYear";
import { isEmailValid } from "./helpers/validateEmail";
import { generateRandomToken } from "./helpers/generateTokens";


export const tokens = async (event: {
    headers: { authorization: string };
    body: string
}): Promise<APIGatewayProxyResult> => {
    try {
        dotenv.config();
        // Verificar si se proporciona el encabezado de autorización
        const authorizationHeader = event.headers?.authorization;

        if (!authorizationHeader?.startsWith('Bearer ')) {
            throw new Error('Token de autorización no proporcionado o en formato incorrecto.');
        }

        if (event.body) {
            const { card_number, cvv, expiration_month, expiration_year, email } = JSON.parse(event.body || '{}');

            if (!card_number || !cvv || !expiration_month || !expiration_year || !email) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Faltan parámetros en la solicitud' }),
                };
            }

            // Validar que el número de tarjeta sea válido
            const isValidCardNumber: boolean = checkCardNumber(card_number);
            // Validar que el CVV sea válido
            const isValidCVV: boolean = isCVVValid(cvv);
            // Validar que el mes de expiración sea válido
            const isValidExpirationMonth: boolean = isMonthValid(expiration_month);
            // Validar que el año de expiración sea válido
            const isValidExpirationYear: boolean = isYearValid(expiration_year);
            // Validar que el correo electrónico sea válido
            const isValidEmail: boolean = isEmailValid(email);

            if (!isValidCardNumber || !isValidCVV || !isValidExpirationMonth || !isValidExpirationYear || !isValidEmail) {
                console.log({isValidCardNumber, isValidCVV, isValidExpirationMonth, isValidExpirationYear, isValidEmail});
                throw new Error('Los datos de la tarjeta no son válidos');
            }

            const randomToken = generateRandomToken(16);
            const expirationDate = new Date();
            expirationDate.setMinutes(expirationDate.getMinutes() + 15);

            const userData = {
                card_number,
                cvv,
                expiration_month,
                expiration_year,
                email,
                token: randomToken,
                expiration: expirationDate.toISOString(),
            };

            const userDataString = JSON.stringify(userData);

            try {
                const redis: Redis = redisCreate;
                const hashTable = 'card_tokens';
                const killTime = 30;
                await CardTokens.create(userData).then(async data => {
                    await redis.setex(`${hashTable}:${randomToken}`, killTime, userDataString);
                    redisQuit(redis);
                }).catch((error) => {
                    console.error('Error al crear el token en postgresql:', error);
                });
                const response = (({ cvv, ...o }) => o)(userData);
                return {
                    statusCode: 200,
                    body: JSON.stringify({  message: 'Token creados correctamente', data: response }),
                }
            } catch (error) {
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: error }),
                };
            }

        } else {
            return {
                statusCode: 400, // Código de estado 400 para una solicitud incorrecta
                body: JSON.stringify({ message: 'El cuerpo de la solicitud está vacío' }),
            };
        }
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error al crear los tokens', error: (error as Error).message }),
        };
    }
};

export const getTokens = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    try {
        const { token } = event.queryStringParameters ?? {};
        if (!token) {
            return {
                statusCode: 400,
                body: JSON.stringify({ message: 'Faltan parámetros en la solicitud' }),
            }
        }

        const redis: Redis = redisCreate;
        const hashTable = 'card_tokens';
        const tokenData = await redis.get(`${hashTable}:${token}`);
        redisQuit(redis);

        if (!tokenData) {
            return {
                statusCode: 404,
                body: JSON.stringify({ message: 'No se encontraron datos para el token proporcionado, o quizás ya expiró.' }),
            };
        }

        const response = (({ cvv, ...o }) => o)(JSON.parse(tokenData));

        return {
            statusCode: 200,
            body: JSON.stringify({ message: 'Datos obtenidos correctamente', data: response }),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Error en el request GET', error: (error as Error).message }),
        };
    }
};

