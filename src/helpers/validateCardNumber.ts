const checkCardNumber = (cardNumber: number): boolean => {
  const lengthOfCard: number = cardNumber.toString().length;
  if (lengthOfCard >= 13 && lengthOfCard <= 16) {
    const digits: number[] = cardNumber.toString().split('').map(Number);
    digits.reverse();
    let sum: number = 0;
    let double: boolean = false;
    digits.forEach((digit: number): void => {
      if (double) {
        digit *= 2;
        if (digit > 9) {
          digit -= 9;
        }
      }
      sum += digit;
      double = !double;
    });
    return sum % 10 === 0;
  } else {
    return false;
  }
};

export {
    checkCardNumber
};