const isMonthValid = (month: string): boolean => {
  const monthNumber = parseInt(month, 10);
  return monthNumber >= 1 && monthNumber <= 12;
}

export { isMonthValid };