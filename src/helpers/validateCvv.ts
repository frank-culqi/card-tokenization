const isCVVValid = (cvv: number): boolean => {
  const cvvPattern = /^\d{3,4}$/;
  return cvvPattern.test(cvv.toString());
}

export { isCVVValid };