const isYearValid = (year: string): boolean => {
  const currentYear = new Date().getFullYear();
  const maxYear = currentYear + 5;

  // Verificar que el año tenga exactamente 4 dígitos
  if (!/^\d{4}$/.test(year)) {
    return false;
  }

  const yearNumber = parseInt(year, 10);
  return yearNumber >= currentYear && yearNumber <= maxYear;
}

export { isYearValid };
