const isEmailValid = (email: string): boolean => {
  const emailPattern = /^[a-zA-Z0-9._%+-]+@(gmail\.com|hotmail\.com|yahoo\.es)$/;

  return email.length >= 5 && email.length <= 100 && emailPattern.test(email);
}

export { isEmailValid };