'use strict';
import Redis from "ioredis";
import dotenv from "dotenv";

dotenv.config();
const redisCreate: Redis = new Redis({
  host: process.env.REDIS_HOST ?? 'localhost',
  port: parseInt(process.env.REDIS_PORT!, 10),
  username: process.env.REDIS_USER ?? 'default',
  password: process.env.REDIS_PASSWORD ?? 'default',
  db: parseInt(process.env.REDIS_DB!, 10) ?? 0,
});
const redisQuit = (redis: Redis) => {
  redis.quit();
}
export {
  redisCreate,
  redisQuit
};