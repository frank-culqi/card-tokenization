import dotenv from 'dotenv';
import { Sequelize } from 'sequelize';

dotenv.config();

const pgsqlConfig = {
  host: process.env.PG_HOST ?? 'localhost',
  database: process.env.PG_DATABASE ?? 'culqi',
  username: process.env.PG_USERNAME ?? 'postgres',
  password: process.env.PG_PASSWORD ?? 'postgres',
  port: process.env.PG_PORT ?? '5432',
};

const sequelize = new Sequelize(pgsqlConfig.database, pgsqlConfig.username, pgsqlConfig.password, {
  host: pgsqlConfig.host,
  port: parseInt(pgsqlConfig.port, 10),
  dialect: 'postgres'
});

export default sequelize;