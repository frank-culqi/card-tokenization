import { DataTypes } from 'sequelize';
import sequelize from '../config/pgsql';

const CardTokens = sequelize.define('CardTokens',
  {
    card_number: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    cvv: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    expiration_month: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    expiration_year: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    expiration: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  }, {
    tableName: "card_tokens",
  }
);

export default CardTokens;